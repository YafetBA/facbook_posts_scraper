
from time import sleep
from selenium import webdriver
from bs4 import BeautifulSoup as bs

from selenium.webdriver.chrome.options import Options
import re
import urllib.parse


from selenium import webdriver


from selenium import webdriver
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
browser = webdriver.Chrome('chromedriver',chrome_options=chrome_options)


def login(username, password):
    browser.get("https://www.facebook.com/login")

    sleep(2)

    email_in = browser.find_element_by_xpath('//*[@name="email"]')
    email_in.send_keys(username)
    sleep(2)
    password_in = browser.find_element_by_xpath('//*[@name="pass"]')
    password_in.send_keys(password)
    sleep(5)
    login_btn = browser.find_element_by_xpath('//*[@name="login"]')
    login_btn.click()

    sleep(2)


def getPage(pageID):
    pageURL = f"https://mbasic.facebook.com/" + pageID
    browser.get(pageURL)


def parseLinks(links):
    """extract unique post URLs"""
    postURLsDict = dict()
    for link in links:
        params = urllib.parse.parse_qs(link[11:])  # removing /story.php?
        postURLsDict[(params["story_fbid"][0], params["id"][0])] = True
    postURLs = list()
    for key in postURLsDict.keys():
        postURLs.append(f"https://mbasic.facebook.com/story.php?story_fbid={key[0]}&id={key[1]}")
    return postURLs


def getLinks(soup, filter=None):
    linkElements = soup.findAll("a")
    rawLinks = list()
    for i in range(len(linkElements)):
        try:
            link = linkElements[i]["href"]
            rawLinks.append(link)
        except:
            pass
    if filter is None:
        return rawLinks
    filteredLinks = list()
    for link in rawLinks:
        if link.startswith(filter):
            filteredLinks.append(link)
    return filteredLinks


def getPostURLs():
    soup = bs(browser.page_source, "html.parser")
    links = getLinks(soup, filter="""/story.php?""")
    return parseLinks(links)


comm_div = re.compile('b+|do')
Comm_text = re.compile('^d')
reac_div = re.compile('d+ d+')
reac_divK=re.compile('c+ c+')
reac_text = re.compile('d+')


def getPost_Infos(posts):
    Post_Info = []
    for post in posts:
        post_details = dict()
        browser.get(post)
        src = browser.page_source
        soup = bs(src, 'lxml')
        # print(soup)
        post_tag = soup.find('div', {'class': 'bm'})

        if post_tag :
            Post = post_tag.find('div').find('p')
            Post = Post.text
            post_details['Post'] = Post

        else :
            post_details['Post'] = ''

        react_tag = soup.find('div', {'class': reac_div})
        react_tagk = soup.find('div', {'class': reac_div})
        if react_tag:
            react = react_tag.find('div', {'class': reac_text})
            reacts = react.text
            post_details['Reactions'] = reacts
        elif react_tagk:
            span=react_tagk.find('span',{'class': 'cx dz'})
            react = span.find('div', {'class': reac_text})
            reacts = react.text
            post_details['Reactions'] = reacts
        else:
            post_details['Reactions'] = ''

        comment_tag = soup.find_all('div', {'class': comm_div})
        if comment_tag is not None:
            comments_List = []
            for comment in comment_tag:
                user = comment.find('h3')
                comment = comment.find('div', {'class': Comm_text})
                if user is not None and comment is not None:
                    comments = {'user': user.text, 'commentaire': comment.text}
                    # print(comment.text,user.text)

                    comments_List.append(comments)
        post_details['Commentaires'] = comments_List
        Post_Info.append(post_details)
    return (Post_Info)