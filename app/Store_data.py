
import psycopg2

def storeData(Post_Infos):
    conn = psycopg2.connect(
        host="localhost",
        database="Facebook",
        user="postgres",
        password="admin")

    cur = conn.cursor()

    for post in Post_Infos[1:]:
        columns = ', '.join('"' + str(x).replace('/', '_').replace('"', ' ') + '"' for x in [*post])
        values = ', '.join("'" + str(x).replace('"', '').replace("'", "") + "'" for x in post.values())
        sql = "INSERT INTO " + '"FacebookPosts"' + " (%s) VALUES ( %s );" % (columns, values)

        cur.execute(sql)
    conn.commit()
    conn.close




