#FROM tiangolo/uvicorn-gunicorn:python3.8-alpine3.10
FROM python:3.8-slim


LABEL maintainer="YafetBENABDALLAH<yafetbenabdallah17045@gmail.com>"


RUN apt-get update && \
    apt-get install -y \
    build-essential \
    python3-dev \
    python3-setuptools \
    git \
    git-crypt \
    unzip \
    chromium-driver \
    gcc \
    make

RUN python -m pip install selenium

# Create a virtual environment in /opt
RUN python3 -m venv /opt/venv && /opt/venv/bin/python -m pip install selenium

RUN apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/

RUN /usr/local/bin/python -m pip install --upgrade pip
COPY requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt
RUN python -m pip install webdriver-manager



COPY /app .

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]