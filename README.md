# **FacebookPosts scraper**

## **1- Introduction:**

This a paython FastApi to extract data (Posts, reactions, Comments) from Facebook public pages


## **2- Requirment for windows**
### Installations:

- [Install](https://www.postgresql.org/download/) Postgresql server.  
- [Install](https://www.anaconda.com/products/distribution) Anaconda. 

## **3- Get Started** 

### Configuration:

- After the installation of postgreSQL configure the password for the postgres database by following these steps:
    - In psql type `\password postgres`.
    - Then type `admin` .
- Create a new dataBase in PostgreSQL  named "Facebook".
- Create a new Table in PostgreSQL  named "FacebookPosts" with columns (Post,Reactions,Commentaires).
- In app/login.txt change username and password by your Facebook account credentials

- In the conda terminal , run the following commands:
    - `conda env create -f environment.yml`
    - `conda activate fcbapi`
    - `cd app`
    - `uvicorn main:app --host 127.0.0.1   `


### Create Docker image:
- In the terminal , run the following command:
    - `  docker build -t scraperapi -f Dockerfile . ` 


### RUN Docker container:
- In the terminal , run the following command:
    - `  docker run -d --name fcbscraperapi -p 8000:8000 scraperapi ` 

      

### Test the API:
 

To test the API : 

- Extract data :  http://127.0.0.1:8000/scraper/pageID

![scraper](/image/scraper.jpg)

- Store data :    http://127.0.0.1:8000/store/pageID

![Store](/image/store.jpg)




